#pragma once

#include "NanoStation.hpp"
#include "../protobuf/vns.pb.h"

namespace nnstation {

struct RovioData {
  double t;
  double pos[3];
  double vel[3];
  double quat[4];
  struct {
    double xx, xy, xz, yy, yz, zz;
  } vel_cov;
  float score;
};

class RovioClient : NanoClient<RovioData> {
 public:
  typedef NanoClient<RovioData> Base;
  typedef Base::mtParsed mtParsed;
  typedef Base::mtQueueParsed mtQueueParsed;
  using Base::connect;
  using Base::msgLen;
  using Base::startRecv;
  using Base::close;
  using Base::mutexQueue_;

  RovioClient() = default;

  ~RovioClient() override = default;

 private:
  bool parseData(uint8_t *pMsg, size_t len, mtParsed &parsed) override {
    vns::Rovio info;
    info.ParseFromArray(pMsg, len);
    parsed.t = info.timestamp();
    parsed.pos[0] = info.position().x();
    parsed.pos[1] = info.position().y();
    parsed.pos[2] = info.position().z();
    parsed.vel[0] = info.velocity().x();
    parsed.vel[1] = info.velocity().y();
    parsed.vel[2] = info.velocity().z();
    parsed.quat[0] = info.quaternion().w();
    parsed.quat[1] = info.quaternion().x();
    parsed.quat[2] = info.quaternion().y();
    parsed.quat[3] = info.quaternion().z();
    parsed.vel_cov.xx = info.velocity_cov().xx();
    parsed.vel_cov.xy = info.velocity_cov().xy();
    parsed.vel_cov.xz = info.velocity_cov().xz();
    parsed.vel_cov.yy = info.velocity_cov().yy();
    parsed.vel_cov.yz = info.velocity_cov().yz();
    parsed.vel_cov.zz = info.velocity_cov().xx();
    parsed.score = info.score();
    return true;
  }

  bool checkQueueAvailable(const mtParsed &parsed, const mtQueueParsed &queueParsed) override {
    return queueParsed.empty() || parsed.t > queueParsed.back().t;
  }
};


class RovioServer : NanoServer<RovioData> {
 public:
  typedef NanoServer<RovioData> Base;
  typedef Base::mtParsed mtParsed;
  using Base::bind;
  using Base::close;

  RovioServer() = default;

  virtual ~RovioServer() = default;

  bool send(const mtParsed &parsed) override {
    vns::Rovio info;
    info.set_timestamp(parsed.t);
    info.mutable_position()->set_x(parsed.pos[0]);
    info.mutable_position()->set_y(parsed.pos[1]);
    info.mutable_position()->set_z(parsed.pos[2]);
    info.mutable_velocity()->set_x(parsed.vel[0]);
    info.mutable_velocity()->set_y(parsed.vel[1]);
    info.mutable_velocity()->set_z(parsed.vel[2]);
    info.mutable_quaternion()->set_w(parsed.quat[0]);
    info.mutable_quaternion()->set_x(parsed.quat[1]);
    info.mutable_quaternion()->set_y(parsed.quat[2]);
    info.mutable_quaternion()->set_z(parsed.quat[3]);
    info.mutable_velocity_cov()->set_xx(parsed.vel_cov.xx);
    info.mutable_velocity_cov()->set_xy(parsed.vel_cov.xy);
    info.mutable_velocity_cov()->set_xz(parsed.vel_cov.xz);
    info.mutable_velocity_cov()->set_yy(parsed.vel_cov.yy);
    info.mutable_velocity_cov()->set_yz(parsed.vel_cov.yz);
    info.mutable_velocity_cov()->set_zz(parsed.vel_cov.zz);
    info.set_score(parsed.score);
    return sendMsg(info.SerializeAsString().c_str(), info.ByteSize());
  }
};

}