#pragma once

#include <iostream>
#include <mutex>
#include <queue>
#include <thread>
#include <string>

#ifdef HC2_VERSION
#include <nn.h>
#include <pubsub.h>
#else
#include <nanomsg/nn.h>
#include <nanomsg/pubsub.h>
#endif

namespace nnstation {

template<typename PARSED>
class NanoClient {
 public:
  typedef PARSED mtParsed;
  typedef std::queue<mtParsed> mtQueueParsed;
  std::mutex mutexQueue_;

  NanoClient() : fd_(-1), msgLen_(-1), timeout_(DBL_MAX) {};

  virtual ~NanoClient() = default;

  bool connect(const std::string &url) {
    url_ = url;
    fd_ = nn_socket(AF_SP, NN_SUB);
    if (fd_ < 0) {
      fprintf(stderr, "nn_socket: %s\n", nn_strerror(nn_errno()));
      return false;
    }
    if (nn_connect(fd_, url_.c_str()) < 0) {
      fprintf(stderr, "nn_socket: %s\n", nn_strerror(nn_errno()));
      close();
      return false;
    }
    /*  We want all messages, so just subscribe to the empty value. */
    if (nn_setsockopt(fd_, NN_SUB, NN_SUB_SUBSCRIBE, "", 0) < 0) {
      fprintf(stderr, "nn_setsockopt: %s\n", nn_strerror(nn_errno()));
      close();
      return false;
    }
    return true;
  }

  inline int &msgLen() {
    return msgLen_;
  }

  inline const int &msgLen() const {
    return msgLen_;
  }

  void startRecv(mtQueueParsed *pQueueParsed) {
    mpQueueParsed_ = pQueueParsed;
    threadLoop_ = std::thread(&NanoClient::recvQueueLoop, this);
  }

  bool recvParsed(mtParsed &parsed) {
    uint8_t *pMsg = nullptr;
    int rc = nn_recv(fd_, &pMsg, NN_MSG, 0);
    if (rc < 0) {
      fprintf(stderr, "nn_recv: %s\n", nn_strerror(nn_errno()));
      return false;
    }
    if (msgLen_ > 0 && rc != msgLen_) {
      fprintf(stderr, "nn_recv: got %d bytes, wanted %d\n", rc, msgLen_);
      return false;
    }
    bool success = parseData(pMsg, rc, parsed);
    nn_freemsg(pMsg);
    return success;
  }

  void recvQueueLoop() {
    mtParsed parsed;
    while (true) {
      if (recvParsed(parsed)) {
        std::lock_guard<std::mutex> lock(mutexQueue_);
        if (checkQueueAvailable(parsed, *mpQueueParsed_)) {
          mpQueueParsed_->push(parsed);
          popTimeoutData(*mpQueueParsed_, timeout_);
        }
      }
    }
  }

  void close() {
    nn_close(fd_);
  }

  void setTimeout(double timeout) {
    timeout_ = timeout;
  }

  virtual bool parseData(uint8_t *pMsg, size_t len, mtParsed &parsed) = 0;

  virtual bool checkQueueAvailable(const mtParsed &parsed, const mtQueueParsed &queueParsed) {
    return true;
  }

  virtual void popTimeoutData(mtQueueParsed &queueParsed, double timeout) {}

 private:
  int fd_;
  std::string url_;
  double timeout_;
  mtQueueParsed *mpQueueParsed_;
  int msgLen_;
  std::thread threadLoop_;
};

/**
 * @brief Basic nanomsg server. Blocked version(Currently).
 * @tparam PARSED
 */
template<typename PARSED>
class NanoServer {
 public:
  typedef PARSED mtParsed;
  typedef std::queue<mtParsed> mtQueueParsed;

  NanoServer() : fd_(-1) {};

  virtual ~NanoServer() = default;

  bool bind(const std::string &url) {
    url_ = url;
    fd_ = nn_socket(AF_SP, NN_PUB);
    if (fd_ < 0) {
      fprintf(stderr, "nn_socket: %s\n", nn_strerror(nn_errno()));
      return false;
    }
    if (nn_bind(fd_, url_.c_str()) < 0) {
      fprintf(stderr, "nn_bind: %s\n", nn_strerror(nn_errno()));
      nn_close(fd_);
      return false;
    }
    return true;
  }

  void close() {
    nn_close(fd_);
  }

  bool sendMsg(const void *pMsg, size_t len) {
    int rc = nn_send(fd_, pMsg, len, 0);
    if (rc < 0) {
      fprintf(stderr, "nn_send: %s (ignoring). rc = %d\n", nn_strerror(nn_errno()), rc);
      return false;
    }
    return true;
  }

  virtual bool send(const mtParsed &parsed) = 0;

 private:
  int fd_;
  std::string url_;
};

}