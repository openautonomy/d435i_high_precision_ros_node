#pragma once

#include "NanoStation.hpp"
#include "../protobuf/fc.pb.h"

namespace nnstation {

struct TimedImuData {
  double t;
  double acc[3];
  double gyr[3];
};

class ImuClient : NanoClient<TimedImuData> {
 public:
  typedef NanoClient<TimedImuData> Base;
  typedef Base::mtParsed mtParsed;
  typedef Base::mtQueueParsed mtQueueParsed;
  using Base::connect;
  using Base::startRecv;
  using Base::close;
  using Base::mutexQueue_;

  ImuClient() = default;

  ~ImuClient() override = default;

 private:
  bool parseData(uint8_t *pMsg, size_t len, mtParsed &parsed) override {
    flight::AttitudeInfo info;
    info.ParseFromArray(pMsg, static_cast<int>(len));
    parsed.t = static_cast<double>(info.timestamp()) / 1e6;
    parsed.gyr[0] = info.angular_rate_nobias_b().x();
    parsed.gyr[1] = info.angular_rate_nobias_b().y();
    parsed.gyr[2] = info.angular_rate_nobias_b().z();
    parsed.acc[0] = info.accel_nobias_b().x();
    parsed.acc[1] = info.accel_nobias_b().y();
    parsed.acc[2] = info.accel_nobias_b().z();
//     printf("IMU: time %11.6fs, [%8.4f, %8.4f, %8.4f], [%8.4f, %8.4f, %8.4f]\n",
//            parsed.t,
//            parsed.gyr[0], parsed.gyr[1], parsed.gyr[2],
//            parsed.acc[0], parsed.acc[1], parsed.acc[2]);
    return true;
  }

  bool checkQueueAvailable(const mtParsed &parsed, const mtQueueParsed &queueParsed) override {
    return queueParsed.empty() || parsed.t > queueParsed.back().t;
  }
};

class ImuServer : NanoServer<TimedImuData> {
 public:
  typedef NanoServer<TimedImuData> Base;
  typedef Base::mtParsed mtParsed;
  using Base::bind;
  using Base::close;

  ImuServer() = default;

  ~ImuServer() override = default;

  bool send(const mtParsed &parsed) override {
    flight::AttitudeInfo info;
    info.set_timestamp(static_cast<uint64_t>(parsed.t * 1e6));
    info.mutable_accel_nobias_b()->set_x(static_cast<float>(parsed.acc[0]));
    info.mutable_accel_nobias_b()->set_y(static_cast<float>(parsed.acc[1]));
    info.mutable_accel_nobias_b()->set_z(static_cast<float>(parsed.acc[2]));
    info.mutable_angular_rate_nobias_b()->set_x(static_cast<float>(parsed.gyr[0]));
    info.mutable_angular_rate_nobias_b()->set_y(static_cast<float>(parsed.gyr[1]));
    info.mutable_angular_rate_nobias_b()->set_z(static_cast<float>(parsed.gyr[2]));
    return sendMsg(info.SerializeAsString().c_str(), static_cast<size_t>(info.ByteSize()));
  }
};

}