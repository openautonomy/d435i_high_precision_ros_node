#pragma once

#include <atomic>
#include <memory>

#include "NanoStation.hpp"
#include "fc.pb.h"

namespace nnstation {

struct TimedFcData {
  double t;
  double pos[3];
  double vel[3];
  double quat[4];
  uint32_t rovio_control_flag = 1;
};

class FcClient : NanoClient<TimedFcData> {
 public:
  typedef NanoClient<TimedFcData> Base;
  typedef Base::mtParsed mtParsed;
  typedef Base::mtQueueParsed mtQueueParsed;
  using Base::connect;
  using Base::startRecv;
  using Base::close;
  using Base::mutexQueue_;

  FcClient() = default;

  virtual ~FcClient() = default;

  std::mutex latestInfoMutex_;
  std::shared_ptr<flight::FC> latestInfo_;

  bool getImu(TimedFcData &timedFcData) {
    mtParsed parsed;
    if (!recvParsed(parsed)) return false;
    timedFcData = parsed;
    return true;
  }

 private:
  bool parseData(uint8_t *pMsg, size_t len, mtParsed &parsed) {
    auto info = std::make_shared<flight::FC>();
    info->ParseFromArray(pMsg, len);
    parsed.t = static_cast<double>(info->timestamp()) / 1e6;
    parsed.rovio_control_flag = info->vns_control().rovio_control_flag();
    parsed.pos[0] = info->navigation_info().position_f().x();
    parsed.pos[1] = info->navigation_info().position_f().y();
    parsed.pos[2] = info->navigation_info().position_f().z();
    parsed.vel[0] = info->navigation_info().velocity_f().x();
    parsed.vel[1] = info->navigation_info().velocity_f().y();
    parsed.vel[2] = info->navigation_info().velocity_f().z();
    parsed.quat[0] = info->navigation_info().q().w();
    parsed.quat[1] = info->navigation_info().q().x();
    parsed.quat[2] = info->navigation_info().q().y();
    parsed.quat[3] = info->navigation_info().q().z();
//    printf("FC: time %11.6fs, [%7.3f, %7.3f, %7.3f], [%7.3f, %7.3f, %7.3f], [%7.4f, %7.4f, %7.4f, %7.4f]\n",
//        parsed.timestamp,
//        parsed.pos[0], parsed.pos[1], parsed.pos[2],
//        parsed.vel[0], parsed.vel[1], parsed.vel[2],
//        parsed.quat[0], parsed.quat[1], parsed.quat[2], parsed.quat[3]);
    latestInfoMutex_.lock();
    latestInfo_ = info;
    latestInfoMutex_.unlock();
    return true;
  }

  bool checkQueueAvailable(const mtParsed &parsed, const mtQueueParsed &queueParsed) {
    return queueParsed.empty() || parsed.t > queueParsed.back().t;
  }
};

}